const calc = {
	value1: "",
	value2: "",
    result: "",
	firstValueEntry: true,
	operationPressed: false,
	memory: "",
    mrcPressed: false,
}
let displayedValue = "";
const btn = document.querySelector(".keys"),
	display = document.querySelector(".display > input")

function show(value, display) {
	display.value = value;
}

function calculate(operation) {
switch (operation) {
		case "+":
			calc.result = parseFloat(calc.value1) + parseFloat(calc.value2);
			break;
		case "-":
			calc.result = parseFloat(calc.value1) - parseFloat(calc.value2);
			break;
		case "*":
			calc.result = parseFloat(calc.value1) * parseFloat(calc.value2);
			break;
		case "/":
			calc.result = parseFloat(calc.value1) / parseFloat(calc.value2);
			break;
	}
	calc.value1 = calc.result;
	calc.value2 = "";
}
btn.addEventListener("click", function (e) {
    switch (true) {
		case /[0-9.]/.test(e.target.value):
			if (calc.firstValueEntry) {
				calc.value1 += e.target.value;
				displayedValue = calc.value1;
			} else {
				calc.value2 += e.target.value;
				displayedValue = calc.value2;
				document.querySelector(`input[value="="]`).removeAttribute("disabled");
			}
			break;
        case /^[\+\-\*\/]$/.test(e.target.value):
			if (calc.operationPressed && calc.value2 != "") {
				calculate(calc.operation);
				displayedValue = calc.result;
			};
			calc.operation = e.target.value;
			calc.firstValueEntry = false;
			calc.operationPressed = true;
			break;
        case /^mrc$/.test(e.target.value):
			if (calc.mrcPressed) {
				document.querySelector('.m').classList.add("hidden");
                    memory = "";
				calc.mrcPressed = false;
			} else {
				calc.value1 = calc.memory;
				displayedValue = calc.value1;
				calc.operationPressed = false;
				calc.mrcPressed = true;
			}
			break;
		case /^m[+-]/.test(e.target.value):
			calc.memory = displayedValue;
			console.log("memory: " + calc.memory);
			document.querySelector('.m').classList.remove("hidden");
			break;
		case e.target.value === "C":
			calc.value1 = "";
            calc.value2 = "";
			calc.result = " ";
			calc.firstValueEntry = true;
			calc.operationPressed = false;
			calc.memory = "";
			calc.mrcPressed = false;
			document.querySelector('.m').classList.add("hidden");
			displayedValue = "";
			break;
        case e.target.value === "=":
			calc.operationPressed = false;
			calculate(calc.operation);
			displayedValue = calc.result;
			break;
	}
            show(displayedValue, display);
});
